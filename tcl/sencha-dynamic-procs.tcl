# /packages/intranet-sencha-tables-rest-procs

ad_library {
    Rest Procedures for the sencha-tables package
    
    @author malte.sussdorff@cognovis.de
}


namespace eval sencha_dynfields:: {}


ad_proc -public sencha_dynfields::elements {
    -object_type:required
    {-object_type_id ""}
    {-page_url "default"}
    {-privilege ""}
    {-display_modes ""}
    {-user_id ""}
} {
    Returns the list of dynfield_attribute names for an object_type in the order given by the page_url
    
    @param object_type Object Type for which we return the attributes
    @param object_type_id Category ID of the subtype for which we want the dynfield attributes
    @param privilege Privilege for which we need to check. Default to "" which means we return all privileges
    @param page_url Page_Url from im_dynfield_layout which contains the sort_order
} {
    if {$object_type_id eq ""} {
        set attribute_sql "
            select aa.attribute_name, da.attribute_id, aa.datatype, aa.min_n_values, aa.max_n_values, aa.pretty_name, da.widget_name, pos_x, pos_y, la.label_style, aa.default_value
            --(select default_value from im_dynfield_type_attribute_map where attribute_id = da.attribute_id limit 1) as default_value
            from im_dynfield_attributes da, acs_attributes aa, im_dynfield_layout la
            where da.acs_attribute_id = aa.attribute_id
            and da.attribute_id = la.attribute_id
            and la.page_url = :page_url
            and object_type = :object_type
            order by pos_y"
    } else {
        if {$display_modes eq ""} {
            set display_where ""
        } else {
            if {[lsearch $display_modes "edit"]>-1} {
                # Make sure that for edit forms the user actually has write permission
                set privilege "write"
            }
            set display_where "and tam.display_mode in ([template::util::tcl_to_sql_list $display_modes])"
        }
        set attribute_sql "
            select aa.attribute_name, da.attribute_id, aa.datatype, aa.min_n_values, aa.max_n_values, aa.pretty_name, da.widget_name, pos_x, pos_y, la.label_style
            from im_dynfield_attributes da, acs_attributes aa, im_dynfield_layout la, im_dynfield_type_attribute_map tam
            where da.acs_attribute_id = aa.attribute_id
            and da.attribute_id = la.attribute_id
            and la.page_url = :page_url
            and tam.object_type_id = :object_type_id
            and tam.attribute_id = da.attribute_id
            $display_where
            order by pos_y"
    }
    
    if {$user_id eq ""} {
        set user_id [ad_conn user_id]
    }

    set data [list]
    db_foreach dynfield $attribute_sql {
	# Translate the pretty name
	set pretty_name [lang::util::localize $pretty_name [lang::user::locale -user_id $user_id]]
        if {$privilege ne ""} {
            if {[im_object_permission -object_id $attribute_id -user_id $user_id -privilege $privilege]} {
               lappend data [list $attribute_name $pretty_name]
            }
        } else {
            if {$min_n_values == 0} {
                set required_p f
            } else {
                set required_p t
            }
            lappend data [list $attribute_name $pretty_name $datatype $pos_x $pos_y $widget_name $label_style $required_p $default_value]
        }
    }
    return $data

}
