# /packages/intranet-sencha-invoices-rest-procs

ad_library {
    Rest Procedures for the sencha-invoices package
    
    @author malte.sussdorff@cognovis.de
}


ad_proc -public im_rest_get_custom_sencha_dynview_columns {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call to get dynview columns
} {

    set rest_otype "im_view"
    set user_id $rest_user_id
    set locale [lang::user::locale -user_id $rest_user_id]

    set error_msg ""
    set total_columns 0
    set view_id 0
    array set query_hash $query_hash_pairs
    
    # If view_name is provided
    if {[exists_and_not_null query_hash(view_name)]} {
        set view_id [im_view_id_from_name $query_hash(view_name)]
    } else {
        set error_msg "You must provide view_id or view_name"
    }
    # If view_id is provided
    if {[exists_and_not_null query_hash(view_id)]} {
        set view_id $query_hash(view_id)
    } else {
        set error_msg "You must provide view_id or view_name"
    }
    
    # Check if such view exist
    db_1row view_checker "select count(*) as count_views from im_views where view_id = :view_id"

    set json_columns ""
    if {$count_views ne 0} {
        set sql_where "v.view_id=:view_id"
        set column_sql "select v.view_id, v.view_name, vc.sort_order, vc.column_name, vc.column_render_tcl, vc.variable_name, vc.datatype, vc.visible_for, vc.ajax_configuration, v.view_label
            from im_view_columns vc
            inner join im_views v on (v.view_id = vc.view_id)
            where $sql_where
            and group_id is null
            order by sort_order
            "
        set obj_ctr 0
        db_foreach column_list_sql $column_sql {
            set temp {}
            set cmd "append temp $visible_for"
            set is_visible_p [eval $cmd]
            if {$is_visible_p ne 0} {
                set komma ",\n"
                if {0 == $obj_ctr} { set komma "" }
                set ajax_configuration_json ""
                append ajax_configuration_json ""
                set ajax_configuration_values [split $ajax_configuration ","]
                set ajax_config_obj_ctrl 0
                foreach ajax_val $ajax_configuration_values {
                    set key_and_value [split $ajax_val ":"]
                    set key [lindex $key_and_value 0]
                    set value [lindex $key_and_value 1]
                    set komma_for_ajax_config ",\n"
                    if {0 == $ajax_config_obj_ctrl} { set komma_for_ajax_config "" }
                    append ajax_configuration_json "$komma_for_ajax_config{\"config_key\": \"[im_quotejson $key]\",\"config_value\": \"$value\"}"
                    incr ajax_config_obj_ctrl
                }
                regsub -all {#} $column_name {} column_name_trimmed
                set column_name_translated "[lang::message::lookup $locale $column_name_trimmed]"
                append json_columns "$komma{\"column_name\": \"[im_quotejson "$column_name"]\", \"column_name_translated\": \"[im_quotejson "$column_name_translated"]\",\"variable_name\": \"[im_quotejson "$variable_name"]\", \"column_render\": \"[im_quotejson "$column_render_tcl"]\",\"datatype\": \"[im_quotejson $datatype]\", \"sort_order\": \"$sort_order\", \"ajax_configuration\": \[\n$ajax_configuration_json\n\]\n}"
                incr obj_ctr
            }
        }
        set total_columns $obj_ctr
    } else {
        set error_msg "View not found"
    }

    if {$count_views ne 0} {
        set result "{\"success\": true,\"view_id\": \"$view_id\", \"view_name\": \"$view_name\", \"view_label\": \"$view_label\", \"total_columns\": \"$total_columns\", \n\"data\": \[\n$json_columns\n\]\n}"
    } else {
        set result "{\"success\": false, \"error_msg\": \"[im_quotejson $error_msg]\", \"total_columns\": \"$total_columns\", \n\"data\": \[\n$json_columns\n\]\n}"
    }
    
    im_rest_doc_return 200 "application/json" $result

}


ad_proc -public im_rest_get_custom_sencha_dynview_rows {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call to get dynview columns
} {

    set user_id $rest_user_id
    array set query_hash $query_hash_pairs

    set view_id 0
    set total_rows 0
    set error_msg ""

    set json_rows ""

    # Default filters
    set project_status_id [im_project_status_open]
    set company_id ""
    set start_date ""
    set end_date ""
    set include_subprojects_p 0
    set mine_p "f"
    set cur_format [im_l10n_sql_currency_format]

    if {"" == $start_date} { set start_date [parameter::get_from_package_key -package_key "intranet-cost" -parameter DefaultStartDate -default "2000-01-01"] }
    if {"" == $end_date} { set end_date [parameter::get_from_package_key -package_key "intranet-cost" -parameter DefaultEndDate -default "2100-01-01"] }
    
    set order_by_clause "order by end_date desc"

    # Building custom variables form ULR (e.g. company_id)
    for {set x 0} {$x<[llength $query_hash_pairs]} {set x [expr {$x+2}]} {
        if {[lindex $query_hash_pairs $x] ne "view_id" && [lindex $query_hash_pairs $x] ne "view_name"} {
            set [lindex $query_hash_pairs $x] [lindex $query_hash_pairs [expr $x+1]]
        }
    }

    if {[exists_and_not_null query_hash(view_name)]} {
        set view_id [im_view_id_from_name $query_hash(view_name)]
    } else {
        set error_msg "You must provide view_id or view_name"
    }

    # If view_id is provided
    if {[exists_and_not_null query_hash(view_id)]} {
        set view_id $query_hash(view_id)
    } else {
        set error_msg "You must provide view_id or view_name"
    }

    # Check if such view exist
    db_1row view_checker "select count(*) as count_views from im_views where view_id = :view_id"

    # Getting View sql, returning error when view_sql not exist
    set view_sql_query [db_0or1row view_sql "select view_sql from im_views where view_id = :view_id"]


    set main_extra_select ""
    set main_extra_from ""
    set main_extra_where ""


    if {true} {
        set rows_sql "select * from im_projects"
        set variables_sql "select extra_select, extra_from, extra_where, variable_name, column_render_tcl, visible_for from im_view_columns where view_id = :view_id"
    
        set obj_ctr 0

        db_foreach view_variables $variables_sql {

            set komma ",\n"
            if {0 == $obj_ctr} { set komma ",\n" }

            if { ![empty_string_p $extra_select] } {

                append main_extra_select "$komma $extra_select"
            }           
            if { ![empty_string_p $extra_from] } {
                set main_extra_from"$komma $extra_from"
            } 
            if { ![empty_string_p $extra_where] } {
                set main_extra_where "$komma $extra_where"
            } 
            incr obj_ctr
            
        }


        set criteria [list]
        if { ![empty_string_p $project_status_id] && $project_status_id > 0 } {
            lappend criteria "p.project_status_id in ([join [im_sub_categories $project_status_id] ","])"
        }
       # if { ![empty_string_p $project_type_id] && $project_type_id != 0 } {
        #    lappend criteria "p.project_type_id in ([join [im_sub_categories $project_type_id] ","])"
        #}

        if { ![empty_string_p $company_id] && $company_id != 0 } {
            lappend criteria "p.company_id=:company_id"
        }

        # Limit to start-date and end-date
        if {"" != $start_date} { lappend criteria "p.end_date::date >= :start_date" }
        if {"" != $end_date} { lappend criteria "p.start_date::date <= :end_date" }


        if { $include_subprojects_p == "f" } {
            lappend criteria "p.parent_id is null"
        }

        set where_clause [join $criteria " and\n            "]
        if { ![empty_string_p $where_clause] } {
            set where_clause " and $where_clause"
        }

        set dept_perm_sql ""
        if {[im_permission $rest_user_id "view_projects_dept"]} {
           set dept_perm_sql "
            UNION
            -- projects of the user department
            select  p.*
            from    im_projects p
            where   p.project_cost_center_id in (select * from im_user_cost_centers(:user_id))
                $where_clause
           "
        }

        set perm_sql "
            (
            -- member projects
            select  p.*
            from    im_projects p,
                acs_rels r
            where   r.object_id_one = p.project_id
                and r.object_id_two in (select :user_id from dual UNION select group_id from group_element_index where element_id = :user_id)
                $where_clause
            $dept_perm_sql
            )
        "


        # User can see all projects - no permissions
        if {[im_permission $user_id "view_projects_all"]} {
           set perm_sql "im_projects"
        }

        # Explicitely looking for the user's projects
        if {"t" == $mine_p} {
            set perm_sql "
            (select p.*
            from    im_projects p,
                acs_rels r
            where   r.object_id_one = p.project_id and 
                r.object_id_two = :user_id
                $where_clause
            )"
        }


        set sql "
        SELECT *
        FROM
                ( SELECT
                        p.*,
                round(p.percent_completed * 10.0) / 10.0 as percent_completed,
                        c.company_id as company_id,
                        c.company_name,
                        im_name_from_user_id(p.project_lead_id) as lead_name,
                        im_category_from_id(p.project_type_id) as project_type,
                        im_category_from_id(p.project_status_id) as project_status,
                        to_char(p.start_date, 'YYYY-MM-DD') as start_date_formatted,
                        to_char(p.end_date, 'YYYY-MM-DD') as end_date_formatted,
                        to_char(p.end_date, 'HH24:MI') as end_date_time
                $main_extra_select
                FROM
                        $perm_sql p,
                        im_companies c
                $main_extra_from
                WHERE
                        p.company_id = c.company_id
                and p.project_type_id not in ([im_project_type_task], [im_project_type_ticket])
                        $where_clause
                $main_extra_where
                ) projects
                $order_by_clause
        "

        set obj_ctr 0
        db_foreach row_sql $sql {
 
            set total_visible_vars 0
            set komma ",\n"
            if {0 == $obj_ctr} { set komma "" }
            set inner_obj_ctrl 1
            set count [llength variables_sql]
            append json_rows "$komma{"
            # First loop to ONLY count visible vars from im_view_columns
            db_foreach variables_sql $variables_sql {
                set is_visible_p "[eval $visible_for]"
                if {$is_visible_p ne 0} {
                    incr total_visible_vars
                }
            }
            if {[exists_and_not_null project_id]} {
                append json_rows "\"project_id\": \"[im_quotejson $project_id]\","
            }
            if {[exists_and_not_null project_lead_id]} {
                append json_rows "\"project_lead_id\": \"[im_quotejson $project_lead_id]\","
            }
            if {[exists_and_not_null company_id]} {
                append json_rows "\"company_id\": \"[im_quotejson $company_id]\","
            }
            # Second loop to collect data (rows)
            db_foreach variables_sql $variables_sql {
                set is_visible_p "[eval $visible_for]"
                if {$is_visible_p ne 0} {
                    eval "set v $$variable_name"
                    #eval "set v $column_render_tcl"
                    
                    if {$inner_obj_ctrl == $total_visible_vars} {
                        append json_rows "\"[im_quotejson $variable_name]\": \"[im_quotejson $v]\""
                    } else {
                        append json_rows "\"[im_quotejson $variable_name]\": \"[im_quotejson $v]\","
                    }
                    incr inner_obj_ctrl
                } 
            }
            append json_rows "}"
            incr obj_ctr
        }
        set total_rows $obj_ctr
        set result "{\"success\": true, \"view_id\": \"$view_id\", \"total_rows\": \"$total_rows\", \n\"data\": \[\n$json_rows\n\]\n}"
        im_rest_doc_return 200 "application/json" $result

    } else {
        set error_msg "view_sql not found"
        set result "{\"success\": false, \"err_ms\": \"$error_msg\"}"
        im_rest_doc_return 200 "application/json" $result
    }

}


ad_proc -public im_rest_get_custom_sencha_dynfields {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call to get dynfiels for given @param page_url
} {

    set user_id $rest_user_id
    array set query_hash $query_hash_pairs

    if {[exists_and_not_null query_hash(obj_type)]} {
       set obj_type $query_hash(obj_type)
    } else {
        set obj_type "none"
    }

    if {[exists_and_not_null query_hash(page_url]} {
       set page_url $query_hash(page_url)
    } else {
        set page_url "default"
    }

    set attributes [sencha_dynfields::elements -object_type $obj_type -page_url $page_url]
    set obj_ctr 0
    set attributes_json ""
    
    foreach var $attributes {
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }

        append attributes_json "$komma{\"attribute_name\": \"[lindex $var 0]\", \"attribute_label\": \"[lindex $var 1]\", \"datatype\": \"[lindex $var 2]\", \"x_position\": \"[lindex $var 3]\", \"y_position\": \"[lindex $var 4]\", \"widget_name\": \"[lindex $var 5]\", \"label_style\": \"[lindex $var 6]\", \"required\": \"[lindex $var 7]\", \"default_value\": \"[lindex $var 8]\"}"
        incr obj_ctr
    }

    set result "{\"success\": true, \"total\": \"$obj_ctr\", \n\"data\": \[\n$attributes_json\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
   
   }