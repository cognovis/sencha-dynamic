-- upgrade-4.1.0.0.0-4.1.0.0.1.sql

SELECT acs_log__debug('/packages/sencha-dynamic/sql/postgresql/upgrade/upgrade-4.1.0.0.0-4.1.0.0.1.sql','');

create or replace function inline_0 ()
returns integer as '
declare
        v_count  integer;
begin
    select count(*) into v_count from im_categories
        where category_id = 1420;
    if v_count > 0 then return 0; end if;
    INSERT INTO im_categories (category_id,category,category_type,aux_string1, aux_string2, enabled_p, parent_only_p)
VALUES (1420,''Sencha'',''Intranet DynView Type'','''','''',''t'',''f'');
    return 0;
end;' language 'plpgsql';

select inline_0 ();
drop function inline_0 ();


delete from im_view_columns where view_id = 1024;
delete from im_views where view_id = 1024;

insert into im_views (view_id, view_name, view_type_id, view_status_id, visible_for, view_sql, sort_order, view_label)
	values (1024, 'sencha_project_list', 1420, NULL, '', ' select p.*, p.project_id, im_category_from_id(p.project_status_id) as project_status, to_char(p.end_date, ''YYYY-MM-DD'') as end_date from im_projects p, im_companies c where p.company_id = c.company_id', 1, 'Projects List');


insert into im_view_columns (column_id, view_id, group_id, column_name, variable_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for, datatype) values (102211, 1024, NULL, 'Project Name','project_name', '"<A HREF=/intranet/projects/view?project_id=$project_id>$project_nr</A>"','','',1,'', 'string');

insert into im_view_columns (column_id, view_id, group_id, column_name, variable_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for, datatype) values (102212, 1024, NULL, 'Delivery Date', 'end_date', '$end_date','','',2,'', 'date');

insert into im_view_columns (column_id, view_id, group_id, column_name, variable_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for, datatype) values (102213, 1024, NULL, 'Status', 'project_status', '$project_status','','',3,'', '');
