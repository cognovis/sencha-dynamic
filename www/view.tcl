ad_page_contract {
    Shows requested view

    @param status_id if specified, limits view to those of this status
    @param type_id   if specified, limits view to those of this type
} {

}

set user_id [ad_maybe_redirect_for_registration]

template::head::add_meta -http_equiv "X-UA-Compatible" -content "IE=edge"
template::head::add_meta -name "viewport" -content "width=device-width, initial-scale=1, maximum-scale=10, user-scalable=yes"

template::head::add_javascript -src "/sencha-dynamic/view.js"

template::head::add_javascript -script "
    var serverParams = {
           serverUrl: '[ad_url]',
    };
    serverParams.restUrl = serverParams.serverUrl + '/intranet-rest/';
    serverParams.auth = '';
"